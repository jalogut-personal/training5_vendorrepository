<?php
/**
 * VendorRepositoryInterface.php
 *
 * @category  Training5
 * @package   Training5_VendorRepositoryInterface
 * @copyright Copyright (c) 2015 Unic AG (http://www.unic.com)
 * @author    juan.alonso@unic.com
 */
namespace Training5\VendorRepository\Api;

/**
 * Vendor CRUD interface.
 */
interface VendorRepositoryInterface
{
    /**
     * Load vendor by Id
     *
     * @api
     * @param string|int $vendorId
     * @return \Training5\VendorRepository\Api\Data\VendorInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If vendor with that id does not exist.
     */
    public function load($vendorId);

    /**
     * Create vendor.
     *
     * @api
     * @param \Training5\VendorRepository\Api\Data\VendorInterface $vendor
     * @return \Training5\VendorRepository\Api\Data\VendorInterface
     * @throws \Magento\Framework\Exception\InputException If bad input is provided
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(\Training5\VendorRepository\Api\Data\VendorInterface $vendor);

    /**
     * Retrieve vendors which match a specified criteria.
     *
     * @api
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Training5\VendorRepository\Api\Data\VendorSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Get associated product ids for vendor.
     *
     * @api
     * @param \Training5\VendorRepository\Api\Data\VendorInterface $vendor
     * @return array|bool $productIds
     * @throws \Magento\Framework\Exception\InputException If bad input is provided
     */
    public function getAssociatedProductIds(\Training5\VendorRepository\Api\Data\VendorInterface $vendor);



}
