<?php
/**
 * VendorSearchResultInterface.php
 *
 * @category  magento2
 * @package   magento2_
 * @copyright Copyright (c) 2015 Unic AG (http://www.unic.com)
 * @author    juan.alonso@unic.com
 */

namespace Training5\VendorRepository\Api\Data;

/**
 * Interface for customer search results.
 */
interface VendorSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get vendors list.
     *
     * @api
     * @return \Training5\VendorRepository\Api\Data\VendorInterface[]
     */
    public function getItems();

    /**
     * Set vendors list.
     *
     * @api
     * @param \Training5\VendorRepository\Api\Data\VendorInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}