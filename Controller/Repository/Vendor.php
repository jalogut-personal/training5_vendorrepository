<?php
/**
 * Vendor.php
 *
 * @category  Training5
 * @package   Training5_VendorRepository
 * @copyright Copyright (c) 2015 Unic AG (http://www.unic.com)
 * @author    juan.alonso@unic.com
 */

namespace Training5\VendorRepository\Controller\Repository;

use Magento\Framework\Api as Api;

class Vendor extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Training5\VendorRepository\Api\VendorRepositoryInterface
     */
    private $vendorRepository;

    /**
     * @var Api\SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var Api\FilterBuilder
     */
    private $filterBuilder;

    /**
     * @var Api\SortOrderBuilder
     */
    private $sortOrderBuilder;

    /**
     * @var Api\Search\FilterGroupBuilder
     */
    private $filterGroupBuilder;

    /**
     * Construct
     *
     * @param \Magento\Framework\App\Action\Context                     $context
     * @param \Training5\VendorRepository\Api\VendorRepositoryInterface $vendorRepository
     * @param Api\SearchCriteriaBuilder                                 $searchCriteriaBuilder
     * @param Api\FilterBuilder                                         $filterBuilder
     * @param Api\Search\FilterGroupBuilder                             $filterGroupBuilder
     * @param Api\SortOrderBuilder                                      $sortOrderBuilder
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Training5\VendorRepository\Api\VendorRepositoryInterface $vendorRepository,
        Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        Api\FilterBuilder $filterBuilder,
        Api\Search\FilterGroupBuilder $filterGroupBuilder,
        Api\SortOrderBuilder $sortOrderBuilder
    ) {
        parent::__construct($context);
        $this->vendorRepository = $vendorRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
    }

    /**
     * Display list of vendors from VendorRepository API
     */
    public function execute()
    {
        $this->getResponse()->setHeader('Content-Type', 'text/plain');
        $vendors = $this->getVendorsFromRepository();

        foreach ($vendors as $vendor) {
            /** @var \Training5\VendorRepository\Api\Data\VendorInterface $vendor */
            $this->getResponse()->appendBody(sprintf('-> ID: %s | NAME: %s ', $vendor->getId(), $vendor->getName()));
        }
    }

    /**
     * Get Vendors from Repository
     *
     * @return \Training5\VendorRepository\Api\Data\VendorInterface[]
     */
    private function getVendorsFromRepository()
    {
        $this->setVendorNameFilter();
        $this->addVendorIdFilters();
        $this->setVendorOrder();

        $searchCriteria = $this->searchCriteriaBuilder->create();
        $this->searchCriteriaBuilder->setFilterGroups(
            [$this->filterGroupBuilder->create()]
        );
        $vendors = $this->vendorRepository->getList($searchCriteria);
        return $vendors->getItems();
    }

    /**
     * Add Name filter
     */
    private function setVendorNameFilter()
    {
        // AND Operator
        $configProductFilter = $this->filterBuilder
            ->setField('name')
            ->setValue('%Vendor A%')
            ->setConditionType('like')
            ->create();
        $this->searchCriteriaBuilder->addFilters([$configProductFilter]);
    }

    /**
     * Add id filters
     */
    private function addVendorIdFilters()
    {
        // OR Operators
        $firstIdFilter = $this->filterBuilder
            ->setField('vendor_id')
            ->setValue('1')
            ->setConditionType('eq')
            ->create();
        $this->filterGroupBuilder->addFilter($firstIdFilter);

        $secondIdFilter = $this->filterBuilder
            ->setField('vendor_id')
            ->setValue('2')
            ->setConditionType('eq')
            ->create();
        $this->filterGroupBuilder->addFilter($secondIdFilter);
    }

    /**
     * Set sorting order
     */
    private function setVendorOrder()
    {
        $sortOrder = $this->sortOrderBuilder
            ->setField('vendor_id')
            ->setDirection(Api\SortOrder::SORT_ASC)
            ->create();
        $this->searchCriteriaBuilder->addSortOrder($sortOrder);
        $this->searchCriteriaBuilder->setPageSize(10);
        $this->searchCriteriaBuilder->setCurrentPage(1);
    }

}