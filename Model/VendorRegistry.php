<?php
/**
 * VendorRegistry.php
 *
 * @category  Training5
 * @package   Training5_VendorRepository
 * @copyright Copyright (c) 2015 Unic AG (http://www.unic.com)
 * @author    juan.alonso@unic.com
 */

namespace Training5\VendorRepository\Model;

use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Registry for \Training4\Vendor\Model\Vendor
 */
class VendorRegistry
{
    const REGISTRY_SEPARATOR = ':';

    /**
     * @var VendorFactory
     */
    private $vendorFactory;

    /**
     * @var array
     */
    private $vendorRegistryById = [];

    /**
     * Constructor
     *
     * @param VendorFactory $vendorFactory
     */
    public function __construct(
        VendorFactory $vendorFactory
    ) {
        $this->vendorFactory = $vendorFactory;
    }

    /**
     * Retrieve Vendor Model from registry given an id
     *
     * @param string $vendorId
     * @return Vendor
     * @throws NoSuchEntityException
     */
    public function retrieve($vendorId)
    {
        if (!isset($this->vendorRegistryById[$vendorId])) {
            /** @var Vendor $vendor */
            $vendor = $this->vendorFactory->create()->load($vendorId);
            if (!$vendor->getId()) {
                // vendor does not exist
                throw NoSuchEntityException::singleField('vendor_id', $vendorId);
            } else {
                $this->vendorRegistryById[$vendorId] = $vendor;
            }
        }
        return $this->vendorRegistryById[$vendorId];
    }

}
