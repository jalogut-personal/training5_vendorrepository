<?php
/**
 * VendorRepository.php
 *
 * @category  Training5
 * @package   Training5_VendorRepository
 * @copyright Copyright (c) 2015 Unic AG (http://www.unic.com)
 * @author    juan.alonso@unic.com
 */

namespace Training5\VendorRepository\Model\Resource;

use Magento\Framework\Api\SortOrder;
use Magento\Framework\Exception\InputException;

/**
 * Vendor repository.
 */
class VendorRepository implements \Training5\VendorRepository\Api\VendorRepositoryInterface
{
    /**
     * @var \Training5\VendorRepository\Model\VendorFactory
     */
    protected $vendorFactory;

    /**
     * @var \Training5\VendorRepository\Model\VendorRegistry
     */
    protected $vendorRegistry;

    /**
     * @var \Magento\Framework\Api\ExtensibleDataObjectConverter
     */
    protected $extensibleDataObjectConverter;

    /**
     * @var \Training4\Vendor\Model\Resource\Vendor
     */
    protected $vendorResourceModel;

    /**
     * @var VendorSearchResultsInterfaceFactory
     */
    protected $searchResultsFactory;

    /**
     * Construct
     *
     * @param \Training5\VendorRepository\Model\VendorFactory      $vendorFactory
     * @param \Training5\VendorRepository\Model\VendorRegistry     $vendorRegistry
     * @param \Magento\Framework\Api\ExtensibleDataObjectConverter $extensibleDataObjectConverter
     * @param \Training4\Vendor\Model\Resource\Vendor              $vendorResourceModel
     */
    public function __construct(
        \Training5\VendorRepository\Model\VendorFactory $vendorFactory,
        \Training5\VendorRepository\Model\VendorRegistry $vendorRegistry,
        \Magento\Framework\Api\ExtensibleDataObjectConverter $extensibleDataObjectConverter,
        \Training4\Vendor\Model\Resource\Vendor $vendorResourceModel,
        \Training5\VendorRepository\Api\Data\VendorSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->vendorFactory = $vendorFactory;
        $this->vendorRegistry = $vendorRegistry;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
        $this->vendorResourceModel = $vendorResourceModel;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function load($vendorId)
    {
        $vendorModel = $this->vendorRegistry->retrieve($vendorId);
        return $vendorModel;
    }

    /**
     * Validate vendor attribute values.
     *
     * @param \Training5\VendorRepository\Api\Data\VendorInterface $vendor
     *
     * @throws InputException
     * @return void
     */
    private function validate(\Training5\VendorRepository\Api\Data\VendorInterface $vendor)
    {
        if (!\Zend_Validate::is(trim($vendor->getName()), 'NotEmpty')) {
            throw new InputException(__(InputException::REQUIRED_FIELD, ['fieldName' => 'name']));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Training5\VendorRepository\Api\Data\VendorInterface $vendor)
    {
        $this->validate($vendor);

        $vendorData = $this->extensibleDataObjectConverter->toNestedArray(
            $vendor,
            [],
            '\Training5\VendorRepository\Api\Data\VendorInterface'
        );
        $vendorModel = $this->vendorFactory->create(['data' => $vendorData]);
        $this->vendorResourceModel->save($vendorModel);

        $savedVendor = $this->load($vendorModel->getId());
        return $savedVendor;
    }

    /**
     * {@inheritdoc}
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        /** @var \Training4\Vendor\Model\Resource\Vendor\Collection $collection */
        $collection = $this->vendorFactory->create()->getCollection();
        //Add filters from root filter group to the collection
        foreach ($searchCriteria->getFilterGroups() as $group) {
            $this->addFilterGroupToCollection($group, $collection);
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $searchCriteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($searchCriteria->getSortOrders() as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getField(),
                    ($sortOrder->getDirection() ?: \Magento\Framework\Api\SortOrder::SORT_ASC)
                );
            }
        }
        $collection->setCurPage($searchCriteria->getCurrentPage());
        $collection->setPageSize($searchCriteria->getPageSize());
        $vendors = [];
        /** @var \Training4\Vendor\Model\Vendor $vendorModel */
        foreach ($collection as $vendorModel) {
            $vendors[] = $vendorModel;
        }
        $searchResults->setItems($vendors);
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function getAssociatedProductIds(\Training5\VendorRepository\Api\Data\VendorInterface $vendor)
    {
        $this->validate($vendor);
        $vendorModel = $this->load($vendor->getId());
        $products = array();
        foreach ($vendorModel->getProducts() as $product) {
            $products[] = $product->getId();
        }
        return $products;
    }

    /**
     * Helper function that adds a FilterGroup to the collection.
     *
     * @param \Magento\Framework\Api\Search\FilterGroup          $filterGroup
     * @param \Training4\Vendor\Model\Resource\Vendor\Collection $collection
     *
     * @return void
     * @throws \Magento\Framework\Exception\InputException
     */
    protected function addFilterGroupToCollection(
        \Magento\Framework\Api\Search\FilterGroup $filterGroup,
        \Training4\Vendor\Model\Resource\Vendor\Collection $collection
    ) {
        $fields = [];
        $conditions = [];
        foreach ($filterGroup->getFilters() as $filter) {
            $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
            $fields[] = $filter->getField();
            $conditions[] = [$condition => $filter->getValue()];
        }
        if ($fields) {
            $collection->addFieldToFilter($fields, $conditions);
        }
    }

}
