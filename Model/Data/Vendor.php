<?php
/**
 * Vendor.php
 *
 * @category  Training5
 * @package   Training5_VendorRepository
 * @copyright Copyright (c) 2015 Unic AG (http://www.unic.com)
 * @author    juan.alonso@unic.com
 */

namespace Training5\VendorRepository\Model\Data;

/**
 * Class Vendor
 */
class Vendor extends \Magento\Framework\Api\AbstractSimpleObject implements \Training5\VendorRepository\Api\Data\VendorInterface
{
    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->_get(self::VENDOR_ID);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->_get(self::NAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setId($id)
    {
        return $this->setData(self::VENDOR_ID, $id);
    }

    /**
     * {@inheritdoc}
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

}
